<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Media Online</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First name:</label><br>
        <input id="fname" type="text" name="fname"><br><br>
        <label for="lname">Last name:</label><br>
        <input id="lname" type="text" name="lname"><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="gen" value="1">Male<br>
        <input type="radio" name="gen" value="2">Female<br>
        <input type="radio" name="gen" value="3">Other<br><br>
        <label>Nationality:</label><br>
        <select name="nation">
            <option value="1">Indonesian</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio:</label><br>
        <textarea id="bio" name="bio" rows="10" cols="30"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>